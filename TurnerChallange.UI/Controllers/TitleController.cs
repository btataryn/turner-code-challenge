﻿using Newtonsoft.Json;
using System.Linq;
using System.Web.Http;
using TurnerChallange.UI.DTOs;
using TurnerChallange.UI.Entities;
using TurnerChallenge.UI.Repository;

namespace TurnerChallange.UI.Services
{
	public class TitleController : ApiController
    {
		private TitlesRepository _repository;
		private TitlesRepository Repository
		{
			get { return _repository ?? (_repository = new TitlesRepository()); }
		}

		[HttpPost]
		public IHttpActionResult FindByName(string titleName)
		{
			var data = Repository.GetTitle(titleName);
			var title = new TitleDto() { TitleName = data.TitleName, TitleId = data.TitleId };
			return Json(title);
		}

		public IHttpActionResult GetTitleDetailById(int id)
		{
			var data = Repository.GetTitleByID(id);
			var genreNames = data.TitleGenres.ToList().ConvertAll<string>(x => x.Genre.Name);
			var participants = data.TitleParticipants.ToList().ConvertAll<Participant>(x => x.Participant);
			var storyLines = data.StoryLines.ToList();
			var titleDetail = new TitleDetail() { GenreNames = genreNames/*, Participants = participants, StoryLines = storyLines*/};
			return Json(titleDetail);
		}
	}
}
