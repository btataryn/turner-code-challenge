﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnerChallange.UI.Entities;

namespace TurnerChallenge.UI.Repository
{
	public class TitlesRepository
	{
		public Title GetTitle(string titleName)
		{
			try
			{
				using (var db = new TitlesContext())
				{
					var titles = db.Titles;
					var title = titles.Single(x => x.TitleName == titleName);
					return title;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	

		public Title GetTitleByID(int titleId)
		{
			try
			{
				using (var db = new TitlesContext())
				{
					var titles = db.Titles;
					var title = titles.Single(x => x.TitleId == titleId);
					return title;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
