﻿var titlesApp = angular.module('titlesApp', ['ngRoute']);

titlesApp.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'Pages/home.html',
		controller: 'mainController'
	})
	.when('/titleSearch', {
		templateUrl: 'Pages/titleSearch.html',
		controller: 'titleSearchController'
	})
	.when('/titleDetail', {
		templateUrl: 'Pages/titleDetail.html',
		controller: 'titleDetailController'
	});
});

titlesApp.controller('mainController', function ($scope) {
	$scope.message = "Welcome to my Turner Code Challenge";
});

titlesApp.controller('titleDetailController', function ($scope, TitleService) {
	var savedData = TitleService.get();
	$scope.titleName = savedData.TitleName;
	$scope.message = $scope.titleName;
	$scope.titleId = savedData.TitleId;
	TitleService.findTitleDetailByID($scope.titleId).then(function (d) {
		$scope.titleDetail = d.data
	}, function (e) {
		alert("Failed to find title:" + e.message);
	});

});

titlesApp.controller('titleSearchController', function ($scope, TitleService) {
	$scope.message = "Search By Title";
	$scope.search = function () {
		TitleService.findByName($scope.titleName).then(function (d) {
			$scope.result = d.data.TitleName;
			TitleService.set(d.data);
	}, function (e) {
		alert("Failed to find title:" + e.message);
	});
	};
});

titlesApp.service('TitleService', function ($http) {
	var SavedData = {};
	this.findByName = function (titleName) {
		var result = $http({
			url: '/api/Title/FindByName',
			method: 'POST',
			params: { titleName: titleName }
		});
		return result;
	};

	this.findTitleDetailByID = function (titleId) {
		var result = $http({
			url: '/api/Title/GetTitleDetailById',
			method: 'POST',
			params: { titleName: titleId }
		});
		return result;
	};

	this.set = function (data) {
		SavedData = data;
	};

	this.get = function () {
		return SavedData;
	};
});
