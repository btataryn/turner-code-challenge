﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.UI.DTOs
{
	public class TitleDto
	{
		public string TitleName { get; set; }
		public int TitleId { get; set; }
	}
}