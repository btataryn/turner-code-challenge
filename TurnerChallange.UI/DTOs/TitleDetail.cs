﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TurnerChallange.UI.Entities;

namespace TurnerChallange.UI.DTOs
{
	public class TitleDetail
	{
		public int TitleId { get; set; }

		#region Genre
		[JsonProperty("GenreNames")]
		public List<string> GenreNames { get; set; }
		#endregion

		/*#region Participants
		[JsonProperty("Participants")]
		public IEnumerable<Participant> Participants { get; set; }
		#endregion

		#region StoryLine
		[JsonProperty("StoryLines")]
		public IEnumerable<StoryLine> StoryLines { get; set; }
		#endregion*/
	}
}